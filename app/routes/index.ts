import { Router } from 'express';

// Routes Names
import { ROUTE_CUSTOMER } from '../GlobalVars';

// Routes Imports
import CustomerRoutes from './CustomerRouter';

const router = Router();

// ROUTES
router.use(`/${ROUTE_CUSTOMER}`, CustomerRoutes);

export default router;
