import express = require('express');
import { Request, Response } from 'express';
import { errorFunctions } from '../utils';
import { customerActions } from '../methods';
import { Customer } from '../models/Customer';
const router = express.Router();

// ROUTER: Customers

// Get by ID
router.get('/:id', async (req: Request, res: Response) => {
  const customerId: number = Number.parseInt(req.params.id);
  if (!customerId) {
    return errorFunctions.manageError('Please provide CustomerID', 400, true, res);
  }
  let result: any;
  await customerActions
    .get(customerId)
    .then((r: Customer) => {
      console.info('Customer', r);
      result = { ...r };
    })
    .catch((err: any) => {
      console.log('Error:', err.message);
      console.log('SQL Query:', err.sql);
    });

  if (!result) {
    return errorFunctions.manageError('Customer not found in DB', 200, false, res);
  } else {
    return res.json(result);
  }
});

// Get all
router.get('/', async (req: Request, res: Response) => {
  let result: any;
  await customerActions
    .getAll()
    .then((r: Customer[]) => {
      result = [ ...r ];
    })
    .catch((err: any) => {
      console.log('Error:', err.message);
      console.log('SQL Query:', err.sql);
    });

  if (!result) {
    return errorFunctions.manageError('Customer not found in DB', 200, false, res);
  } else {
    return res.json(result);
  }
});

// Post
router.post('/', async (req: Request, res: Response) => {
  const body = req.body;
  const customerToAdd = new Customer();
  customerToAdd.ID = body.ID;
  customerToAdd.PhoneNumber = body.PhoneNumber;

  if (!customerToAdd) {
    return errorFunctions.manageError('Please provide CustomerID', 400, true, res);
  }

  await customerActions
    .create(customerToAdd)
    .then((r: Customer) => {
      console.info('Customer:', r.ID);
      return res.json(r);
    })
    .catch((err: any) => {
      console.log('Error:', err.message);
      console.log('SQL Query:', err.sql);
      return errorFunctions.manageError(err.message, 400, true, res);
    });
});

export default router;
