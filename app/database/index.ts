import { Sequelize, Options } from 'sequelize';
import { customerDbModel, sequelizeCustomerOptions, Customer } from '../models/Customer';
import { customerTypeDbModel, sequelizeCustomerTypeOptions, CustomerType } from '../models/CustomerType';
import { DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS } from '../GlobalVars';

const options: Options = {
  host: DB_HOST,
  port: DB_PORT,
  dialect: 'mssql',
  pool: {
    max: 5,
    min: 0,
    idle: 10000,
  },
  dialectOptions: {
    options: {
      encrypt: true,
    },
  },
  logging: false
};

console.log(`Connecting to ${DB_NAME}`)
export const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, options);

Customer.init(customerDbModel, { ...sequelizeCustomerOptions, sequelize });
CustomerType.init(customerTypeDbModel, { ...sequelizeCustomerTypeOptions, sequelize });

Customer.belongsTo(CustomerType);
CustomerType.hasMany(Customer, {
  sourceKey: 'Id',
  foreignKey: 'CustomerTypeId',
  as: 'customers',
});

export async function syncTables() {
  await sequelize.sync({ alter: true, logging: false });
  console.log('All models were synchronized successfully.');
}
