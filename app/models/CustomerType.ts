import { DataTypes, Model, ModelAttributes, Association } from 'sequelize';
import { Customer } from './Customer';
import { CUSTOMER_TYPE } from '../GlobalVars';

export class CustomerType extends Model {
  public Id!: number;
  public Type!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public readonly customers?: Customer[];

  public static associations: {
    customers: Association<CustomerType, Customer>;
  };
}

export const customerTypeDbModel: ModelAttributes = {
  Id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true, autoIncrementIdentity: true },
  Type: { type: DataTypes.INTEGER, allowNull: false },
};

export const sequelizeCustomerTypeOptions = {
  tableName: CUSTOMER_TYPE,
};
