import { DataTypes, Model, ModelAttributes } from 'sequelize';
import { CUSTOMER } from '../GlobalVars';

export class Customer extends Model {
  public ID!: number;
  public PhoneNumber!: string;
  public readonly CustomerTypeId!: number;

  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export const customerDbModel: ModelAttributes = {
  ID: { type: DataTypes.BIGINT, primaryKey: true },
  PhoneNumber: { type: DataTypes.STRING, allowNull: false },
  CustomerTypeId: { type: DataTypes.INTEGER, allowNull: true },
};

export const sequelizeCustomerOptions = {
  tableName: CUSTOMER,
};
