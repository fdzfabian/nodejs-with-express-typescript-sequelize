import app from './App';
import { sequelize, syncTables } from './database/index';
import { API_PORT, LOG_SEPARATOR } from './GlobalVars';
import { printRoutes } from './utils/loggerHelper';

app.listen(API_PORT, async () => {
  console.log(LOG_SEPARATOR);
  console.log(`API listening on port ${API_PORT}`);
  console.log(LOG_SEPARATOR);
  await sequelize
    .authenticate()
    .then(async () => {
      console.log('Connected to DBaaS');
      await syncTables();
    })
    .catch(err => {
      console.log('Error connecting with DBaaS. Error:', err);
    });
  console.log(LOG_SEPARATOR);
  printRoutes(app);
});
