import { Response } from 'express';

export function manageError(errorMsg: string, code: number, isError: boolean, res: Response) {
  return res.status(400).json({
    isError,
    messsage: errorMsg,
    code,
  });
}
