import * as dotenv from 'dotenv';
import * as env from 'env-var';
dotenv.config();

// Database Variables
export const DB_HOST: string = env
  .get('DB_HOST')
  .required()
  .asString();
export const DB_NAME: string = env
  .get('DB_NAME')
  .required()
  .asString();
export const DB_USER: string = env
  .get('DB_USER')
  .required()
  .asString();
export const DB_PASS: string = env
  .get('DB_PASS')
  .required()
  .asString();
export const DB_PORT: number = env
  .get('DB_PORT')
  .required()
  .asPortNumber();

// Table Names
export const CUSTOMER = 'Customer';
export const CUSTOMER_TYPE = 'CustomerType';

// Routes Names
export const ROUTE_CUSTOMER = 'customer';
export const ROUTE_CUSTOMER_TYPE = 'customerType';

// API Variables
export const API_PORT: number = env
  .get('API_PORT')
  .required()
  .asPortNumber();

// Logging Variables
export const LOG_SEPARATOR = '===========================================================';
