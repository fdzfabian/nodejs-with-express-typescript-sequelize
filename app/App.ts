import express from 'express';
import * as bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';

import routes from './routes';
import { LOG_SEPARATOR } from './GlobalVars';

class App {
  public app: express.Application;

  constructor() {
    // Run the express instance and store in app
    this.app = express();
    this.config();
  }

  private async config(): Promise<void> {
    // Middlewares
    this.app.use(cors());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: false }));

    // Logger service
    this.app.use(
      morgan(
        `${LOG_SEPARATOR}\n[:method] :date \nURL\: :url\nSTATUS\: :status\nRESPONSE TIME: :response-time ms`
      )
    );

    // Routes
    this.app.use('/api/v1', routes);
  }
}

export default new App().app;
