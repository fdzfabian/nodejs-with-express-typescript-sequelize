import { Customer } from '../models/Customer';

export async function get(ID: number): Promise<any> {
  return await Customer.findAll({
    where: {
      ID,
    },
  });
}

export async function getAll(): Promise<any> {
  return await Customer.findAll();
}

export async function create(clienteToAdd: Customer): Promise<any> {
  console.log('Customer to add:', clienteToAdd.ID);
  return await clienteToAdd.save();
}

export function validate(body: any) {
  // if (body.)...
}