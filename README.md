# API | TypeScript

This is an API made with TypeScript.

## Setting up

After use it, you need to create an `.env` file with next values:

- DB_HOST: Database host.
- DB_NAME: Database name.
- DB_USER: Database user.
- DB_PASS: Database password.
- DB_PORT: Port used to connect with DB host.
- API_PORT: Port where the API will be running.

To run it (after add `.env` file), just need to run follow commands in your `cmd`:

```bash
    # Install dependencies
    npm install

    # Run in DEVELOPMENT
    npm run dev

    # Run in PRODUCTION
    npm run dev
```
